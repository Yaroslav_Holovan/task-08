package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private int temperature;
    private String lightRequiring;
    private int wateringPerWeek;
    private String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public int getWateringPerWeek() {
        return wateringPerWeek;
    }

    public void setWateringPerWeek(int wateringPerWeek) {
        this.wateringPerWeek = wateringPerWeek;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}
