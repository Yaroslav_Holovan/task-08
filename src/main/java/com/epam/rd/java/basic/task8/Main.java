package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowerList = domController.getFlowers();


		// sort (case 1)
		flowerList.sort(Comparator.comparing(Flower::getName));
		
		// save
		String outputXmlFile = "output.dom.xml";
		XMLSaverController.writeXmlFile(flowerList, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowers = saxController.getFlowers();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowers.sort(Comparator.comparing(Flower::getAveLenFlower).reversed());
		// save
		outputXmlFile = "output.sax.xml";
		XMLSaverController.writeXmlFile(flowers, outputXmlFile);
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		flowers = staxController.getFlowers();


		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowers.sort(Comparator.comparing(Flower::getTemperature));

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		XMLSaverController.writeXmlFile(flowers, outputXmlFile);
	}

}
