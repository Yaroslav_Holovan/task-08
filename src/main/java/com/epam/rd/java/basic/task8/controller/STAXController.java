package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getFlowers() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		xmlInputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		XMLEventReader xmlReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
		List<Flower> flowers = new ArrayList<>();
		Flower flower = new Flower();

		while(xmlReader.hasNext()){
			XMLEvent event = xmlReader.nextEvent();

			if(event.isStartElement()){
				StartElement startElement = event.asStartElement();

				switch (startElement.getName().getLocalPart()){
					case "flower":
						flower = new Flower();
						break;
					case "name":
						event = xmlReader.nextEvent();
						String name = event.asCharacters().getData();
						flower.setName(name);
						break;
					case "soil":
						event = xmlReader.nextEvent();
						flower.setSoil(event.asCharacters().getData());
						break;
					case "origin":
						event = xmlReader.nextEvent();
						flower.setOrigin(event.asCharacters().getData());
						break;
					case "stemColour":
						event = xmlReader.nextEvent();
						flower.setStemColour(event.asCharacters().getData());
						break;
					case "leafColour":
						event = xmlReader.nextEvent();
						flower.setLeafColour(event.asCharacters().getData());
						break;
					case "aveLenFlower":
						event = xmlReader.nextEvent();
						flower.setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
						break;
					case "temperature":
						event = xmlReader.nextEvent();
						flower.setTemperature(Integer.parseInt(event.asCharacters().getData()));
						break;
					case "watering":
						event = xmlReader.nextEvent();
						flower.setWateringPerWeek(Integer.parseInt(event.asCharacters().getData()));
						break;
					case "multiplying":
						event = xmlReader.nextEvent();
						flower.setMultiplying(event.asCharacters().getData());
						break;
					default:
				}
			}
			if(event.isEndElement()){
				EndElement endElement = event.asEndElement();
				if(endElement.getName().getLocalPart().equals("flower")){
					flowers.add(flower);
				}
			}
		}
		return flowers;
	}

	// PLACE YOUR CODE HERE

}